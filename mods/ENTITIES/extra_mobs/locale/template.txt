# textdomain:extra_mobs
Hoglin=
piglin=
piglin Brute=
Strider=
Fox=
Cod=
Salmon=
dolphin=
Glow Squid=
Glow Ink Sac=
Use it to craft the Glow Item Frame.=
Use the Glow Ink Sac and the normal Item Frame to craft the Glow Item Frame.=
Glow Item Frame=
Can hold an item and glows=
Glow Item frames are decorative blocks in which items can be placed.=
Just place any item on the item frame. Use the item frame again to retrieve the item.=